/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "xmodem.h"
#include "flash.h"
#include "STM_UART.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART1_UART_Init(void);
/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

volatile unsigned char RXbuf[100];
char Seting1[20],Seting2[20],Seting3[20], Set1[10];
char sbuf[100], subrec[100];
int U1_ReadCount;

volatile unsigned long currentMillis;
unsigned long TickMillis;
unsigned long TickDelay = 5000; //6s

unsigned long dotMillis;
unsigned long dotDelay = 1000; //5s

//*********************** String function *******************************

int indexOf(char *str,char *str_Search){
	char *b = strstr(str,str_Search);
	int position = b - str;
	return(position);
}

char* substring(char *destination, const char *source, int beg, int n)
{
	while (n > 0)
	{
		*destination = *(source + beg);

		destination++;
		source++;
		n--;
	}
	// null terminate destination string
	*destination = '\0';
	// return the destination string
	return destination;
}

//**********************************************************************

char gen_checksum(char* str)
{
	// between $ and * include $
	unsigned char checksum = 0;
	int i = 0;

	while(((*str) != 0) && ((*str) != '*') && i <2000)
	{
		//if((*str) != '$')
		checksum ^= (*str++);
		i++;
	}
	return checksum;
}


/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */
  /* Send welcome message on startup. */
	__HAL_UART_ENABLE_IT(&huart1, UART_IT_RXNE); // flag receive
	HAL_NVIC_SetPriority(USART1_IRQn, 3, 0);
	HAL_NVIC_EnableIRQ(USART1_IRQn);

//  uart_transmit_str((uint8_t*)"\n\r================================\n\r");
//  uart_transmit_str((uint8_t*)"UART Bootloader\n\r");
//  uart_transmit_str((uint8_t*)"https://github.com/ferenc-nemeth\n\r");
//  uart_transmit_str((uint8_t*)"================================\n\r\n\r");
	sprintf(sbuf,"Honey Toast : bootloader Version %s\n",BootloaderVer);
	Serial1_Println(sbuf);

	Serial1_Println("==== Please confirm within 5 minute ====");
	Serial1_Println("-> New firmware : [Y] or [N]");

  while(1){
	  currentMillis = HAL_GetTick();
		if(Serial1_available() > 0){
			int len = Serial1_readByte_String(RXbuf,40);
			sprintf(sbuf,"-> [%d]RX1:%s",len,RXbuf);
			Serial1_Println(sbuf);

			strcpy(subrec,(char*)RXbuf);

			subrec[strlen((char*)RXbuf)] = ',';
			subrec[strlen((char*)RXbuf)+1] = '\r';
			subrec[strlen((char*)RXbuf)+2] = '\n';

			substring(Seting1, subrec, 0, indexOf(subrec,","));
			substring(subrec, subrec, indexOf(subrec,",")+1, strlen(subrec)+1);

			substring(Seting2, subrec, 0, indexOf(subrec,","));
			substring(subrec, subrec, indexOf(subrec,",")+1, strlen(subrec)+1);

			substring(Seting3, subrec, 0, indexOf(subrec,","));
			substring(subrec, subrec, indexOf(subrec,",")+1, strlen(subrec)+1);

			if((strcmp(Seting1, "Y") == 0) || (strcmp(Seting1, "y") == 0)){
				Serial1_Println("Yes");
				break;
			}
			else if((strcmp(Seting1, "N")==0) || (strcmp(Seting1, "n") == 0)){
				Serial1_Println("No");
			    uart_transmit_str((uint8_t*)"Jumping to user application...\n\r");
			    flash_jump_to_app();
			}
			else if(strcmp(Seting1, "$FWUP")==0){   //EX: $FWUP,H028,1*
				Serial1_Println("New firmware update!");

				substring(Set1, Seting3, 0, indexOf(Seting3,"*"));

//				sprintf(sbuf,"-> Seting2:%s, Seting3:%s, Set1:%s",Seting2, Seting3, Set1);
//				Serial1_Println(sbuf);

				if(strcmp(Set1, "1") == 0){
					sprintf(sbuf,"$FWUP,ACK,1");
					char chksum = gen_checksum(sbuf);
					sprintf(sbuf,"%s*%02X*#",sbuf,chksum);
					Serial1_Println(sbuf);
					break;
				}
			}
			else{
				Serial1_Println("Command not found!");
			}
		}

	  if ((unsigned long)(currentMillis - TickMillis) >= TickDelay) {
		  Serial1_Println("Timeout 5 sec");
		  uart_transmit_str((uint8_t*)"Jumping to user application...\n\r");
		  flash_jump_to_app();
	  	}

	  if ((unsigned long)(currentMillis - dotMillis) >= dotDelay) {
		  Serial1_Print("= ");
		  dotMillis = currentMillis;
	  }

  }

  Serial1_Println("");

  /* If the button is pressed, then jump to the user application,
   * otherwise stay in the bootloader. */
//  if(!HAL_GPIO_ReadPin(BTN_GPIO_Port, BTN_Pin))
//  {
//    uart_transmit_str((uint8_t*)"Jumping to user application...\n\r");
//    flash_jump_to_app();
//  }
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* Turn on the green LED to indicate, that we are in bootloader mode.*/
    HAL_GPIO_WritePin(GPIOC, LD3_Pin, GPIO_PIN_SET);
    /* Ask for new data and start the Xmodem protocol. */
    uart_transmit_str((uint8_t*)"Please send a new binary file with Xmodem protocol to update the firmware.\n\r");
    xmodem_receive();
    /* We only exit the xmodem protocol, if there are any errors.
     * In that case, notify the user and start over. */
    uart_transmit_str((uint8_t*)"\n\rFailed... Please try again.\n\r");
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSE;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, LD3_Pin|LD2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : BTN_Pin */
  GPIO_InitStruct.Pin = BTN_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(BTN_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LD3_Pin LD2_Pin */
  GPIO_InitStruct.Pin = LD3_Pin|LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
